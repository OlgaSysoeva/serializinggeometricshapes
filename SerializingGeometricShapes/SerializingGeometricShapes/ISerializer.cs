﻿using SerializingGeometricShapes.Figures;

namespace SerializingGeometricShapes
{
    /// <summary>
    /// Интерфес с методами сериализации разных фигур.
    /// </summary>
    public interface ISerializer
    {
        /// <summary>
        /// Сериализует данные объекта с типом Point.
        /// </summary>
        /// <param name="point">Объект типа Point.</param>
        /// <returns>Возвращает строковое представление объекта.</returns>
        string Serialize(Point point);

        /// <summary>
        /// Сериализует данные объекта с типом Circle.
        /// </summary>
        /// <param name="point">Объект типа Circle.</param>
        /// <returns>Возвращает строковое представление объекта.</returns>
        string Serialize(Circle circle);

        /// <summary>
        /// Сериализует данные объекта с типом Square.
        /// </summary>
        /// <param name="point">Объект типа Square.</param>
        /// <returns>Возвращает строковое представление объекта.</returns>
        string Serialize(Square square);
    }
}
