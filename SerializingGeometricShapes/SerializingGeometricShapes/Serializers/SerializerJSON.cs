﻿using SerializingGeometricShapes.Figures;
using System;

namespace SerializingGeometricShapes
{
    /// <summary>
    /// Класс-сериализатор в тип JSON.
    /// </summary>
    class SerializerJSON : ISerializer
    {
        public string Serialize(Point point)
        {
            if (point == null)
            {
                throw new ArgumentNullException(nameof(point));
            }

            return $"{{\n  \"{nameof(point)}\":{{" +
                $"\n    \"{nameof(point.X)}\":\"{point.X}\"," +
                $"\n    \"{nameof(point.Y)}\":\"{point.Y}\"\n  }}\n}}\n";
        }

        public string Serialize(Circle circle)
        {
            if (circle == null)
            {
                throw new ArgumentNullException(nameof(circle));
            }

            return $"{{\n  \"{nameof(circle)}\":{{" +
                $"\n    \"{nameof(circle.X)}\": \"{circle.X}\"," +
                $"\n    \"{nameof(circle.Y)}\": \"{circle.Y}\"," +
                $"\n    \"{nameof(circle.R)}\": \"{circle.R}\"\n  }}\n}}\n";
        }

        public string Serialize(Square square)
        {
            if (square == null)
            {
                throw new ArgumentNullException(nameof(square));
            }

            return $"{{\n  \"{nameof(square)}\":{{" +
                $"\n    \"{nameof(square.X)}\": \"{square.X}\"," +
                $"\n    \"{nameof(square.Y)}\": \"{square.Y}\"," +
                $"\n    \"{nameof(square.W)}\": \"{square.W}\"\n  }}\n}}\n";
        }
    }
}
