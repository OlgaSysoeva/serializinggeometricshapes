﻿using System;

namespace SerializingGeometricShapes.Figures
{
    /// <summary>
    /// Класс представляет точку оси координат.
    /// </summary>
    public class Point
    {
        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }

        /// <summary>
        /// Значение оси Х. 
        /// </summary>
        public readonly int X;

        /// <summary>
        /// Значение оси У.
        /// </summary>
        public readonly int Y;

        /// <summary>
        /// Вызов сериализации объекта.
        /// </summary>
        /// <param name="context">Контекст содержащий тип сериализации.</param>
        /// <returns>Возвращает строковое представление объекта в соответствии с типом сериализации.</returns>
        public virtual string Serialize(Context context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            return context.Serialize(this);
        }
    }
}
