﻿using System;

namespace SerializingGeometricShapes.Figures
{
    /// <summary>
    /// Класс представляет фигуру круг.
    /// </summary>
    public class Circle : Point
    {
        public Circle(int x, int y, int r)
            : base(x, y)
        {
            R = r;
        }

        /// <summary>
        /// Радиус круга.
        /// </summary>
        public readonly int R;

        public override string Serialize(Context context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            return context.Serialize(this);
        }
    }
}
