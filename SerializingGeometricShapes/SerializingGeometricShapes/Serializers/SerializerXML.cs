﻿using SerializingGeometricShapes.Figures;
using System;

namespace SerializingGeometricShapes
{
    /// <summary>
    /// Класс-сериализатор в тип XML.
    /// </summary>
    class SerializerXML : ISerializer
    {
        public string Serialize(Point point)
        {
            if (point == null)
            {
                throw new ArgumentNullException(nameof(point));
            }

            return $"<{nameof(point)}>" +
                $"\n  <{nameof(point.X)}>{point.X}</{nameof(point.X)}>" +
                $"\n  <{nameof(point.Y)}>{point.Y}</{nameof(point.Y)}>" +
                $"\n</{nameof(point)}>\n";
        }

        public string Serialize(Circle circle)
        {
            if (circle == null)
            {
                throw new ArgumentNullException(nameof(circle));
            }

            return $"<{nameof(circle)}>" +
                $"\n  <{nameof(circle.X)}>{circle.X}</{nameof(circle.X)}>" +
                $"\n  <{nameof(circle.Y)}>{circle.Y}</{nameof(circle.Y)}>" +
                $"\n  <{nameof(circle.R)}>{circle.R}</{nameof(circle.R)}>" +
                $"\n</{nameof(circle)}>\n";
        }

        public string Serialize(Square square)
        {
            if (square == null)
            {
                throw new ArgumentNullException(nameof(square));
            }

            return $"<{nameof(square)}>" +
                $"\n  <{nameof(square.X)}>{square.X}</{nameof(square.X)}> " +
                $"\n  <{nameof(square.Y)}>{square.Y}</{nameof(square.Y)}> " +
                $"\n  <{nameof(square.W)}>{square.W}</{nameof(square.W)}>" +
                $"\n</{nameof(square)}>\n";
        }
    }
}
