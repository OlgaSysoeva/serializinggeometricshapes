﻿using System;

namespace SerializingGeometricShapes.Figures
{
    /// <summary>
    /// Представляет фигуру квадрат.
    /// </summary>
    public class Square : Point
    {
        public Square(int x, int y, int w)
            : base(x, y)
        {
            W = w;
        }

        /// <summary>
        /// Длина стороны квадрата.
        /// </summary>
        public readonly int W;

        public override string Serialize(Context context)
        {
            if (context == null)
            {
                throw new ArgumentNullException();
            }

            return context.Serialize(this);
        }
    }
}
