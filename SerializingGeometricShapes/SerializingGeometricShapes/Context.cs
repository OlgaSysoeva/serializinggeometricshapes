﻿using SerializingGeometricShapes.Figures;

namespace SerializingGeometricShapes
{
    /// <summary>
    /// Контекст определяющий интерфейс для сериализации клиентов.
    /// </summary>
    public class Context
    {
        public Context()
        {
        }

        /// <summary>
        /// Ссылка на объект сериализации.
        /// </summary>
        private ISerializer _serializer;

        /// <summary>
        /// Устанавливает тип сериализации.
        /// </summary>
        /// <param name="serializer">Объект сериализации.</param>
        public void SetStrategy(ISerializer serializer)
        {
            _serializer = serializer;
        }

        /// <summary>
        /// Сериализация данных объекта с типом Point.
        /// </summary>
        /// <param name="point">Объект типа Point.</param>
        /// <returns>Возвращает строковое представление объекта в соответствии с типом сериализации.</returns>
        public string Serialize(Point point)
        {
            return _serializer.Serialize(point);
        }

        /// <summary>
        /// Сериализация данных объекта с типом Circle.
        /// </summary>
        /// <param name="point">Объект типа Circle.</param>
        /// <returns>Возвращает строковое представление объекта в соответствии с типом сериализации.</returns>
        public string Serialize(Circle circle)
        {
            return _serializer.Serialize(circle);
        }

        /// <summary>
        /// Сериализация данных объекта с типом Square.
        /// </summary>
        /// <param name="point">Объект типа Square.</param>
        /// <returns>Возвращает строковое представление объекта в соответствии с типом сериализации.</returns>
        public string Serialize(Square square)
        {
            return _serializer.Serialize(square);
        }
    }
}
