﻿using SerializingGeometricShapes.Figures;
using System;

namespace SerializingGeometricShapes
{
    //Применены паттерны Стратегия и Посетитель.
    class Program
    {
        static void Main()
        {
            Point point = new Point(2, 5);
            Point circle = new Circle(10, 15, 5);
            Point square = new Square(18, 30, 8);

            var context = new Context();

            context.SetStrategy(new SerializerJSON());

            Console.WriteLine("Сериализация JSON\n");
            Console.WriteLine(point.Serialize(context));
            Console.WriteLine(circle.Serialize(context));
            Console.WriteLine(square.Serialize(context));

            context.SetStrategy(new SerializerXML());

            Console.WriteLine("\nСериализация XML\n");
            Console.WriteLine(point.Serialize(context));
            Console.WriteLine(circle.Serialize(context));
            Console.WriteLine(square.Serialize(context));
        }
    }

    //Преимуществом паттерна Стратегия в том, что в любой момент
    // можно изменить стратегию (класс) на необходимый не пересоздавая объект класса Context.

    //Преимущества паттерна Посетитель в том, что каждый класс имеет свои особенности,
    // но применяется ко всем одна операция.
    
    //Недостатком применения этих шаблонов в данном случае привело к многочисленным классам и методам
    // в каждом классе. Чем больше типов посетителей и стратегий, тем больше классов и методов.
}
